package sh.mio9.android.books.ui.shoppingcart;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import sh.mio9.android.books.MainActivity;
import sh.mio9.android.books.R;

public class ShoppingCartFragment extends Fragment implements View.OnClickListener{

    private ShoppingCartViewModel shoppingCartViewModel;

    AlertDialog.Builder builder;

    Button checkoutButton;
    Button removeButton;
    Boolean dialogAns;
    CardView bookCard;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_shopping_cart, container, false);
        removeButton = root.findViewById(R.id.remove);
        checkoutButton = root.findViewById(R.id.checkoutButton);
        removeButton.setOnClickListener(this);
        checkoutButton.setOnClickListener(this);
        bookCard = root.findViewById(R.id.bookCard);

        return root;
    }

    @Override
    public void onClick(View v) {
//        Fragment fragment= new Fragment(R.id.nav_checkout);
//        FragmentManager fragmentManager = getFragmentManager();
//        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//        fragmentTransaction.replace(R.id.nav_shopping_cart, fragment);
//        fragmentTransaction.commit();
//        if (v.getId()==R.id.remove){
//            showDialog();
//
//        }
//        if (v.getId()==R.id.checkoutButton){
//
//        }
    }

    private void showDialog(){
        builder = new AlertDialog.Builder(getView().getContext());
        builder.setMessage("Are you sure?");
        builder.setCancelable(false);

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialogAns = true;
                bookCard.setVisibility(View.GONE);
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialogAns = false;
            }
        });

        AlertDialog alert = builder.create();
        alert.setTitle("Confirmation");
        alert.show();
    }
}
