package sh.mio9.android.books.ui.shoppingcart;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class ShoppingCartViewModel extends ViewModel {


    private MutableLiveData<String> mText;

    public ShoppingCartViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is shopping cart fragment");
    }

    public MutableLiveData<String> getText() {
        return mText;
    }
}