package sh.mio9.android.books.data;

import sh.mio9.android.books.data.model.LoggedInUser;

import java.io.Externalizable;
import java.io.IOException;

/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */
public class LoginDataSource {

    public Result<LoggedInUser> login(String username, String password) {

        try {
            // TODO: handle loggedInUser authentication
            if (!(username.equals("admin@mio9.sh")||username.equals("mio9")||username.equals("test"))){
                throw new Exception("Incorrect credential");
            }
            LoggedInUser fakeUser =
                    new LoggedInUser(
                            java.util.UUID.randomUUID().toString(),
                            "MIO Nekoya");
            return new Result.Success<>(fakeUser);
        } catch (Exception e) {
            return new Result.Error(new IOException("Error logging in", e));
        }
    }

    public void logout() {
        // TODO: revoke authentication
    }
}
