package sh.mio9.android.books.ui.home;

import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;

import java.util.ArrayList;

import sh.mio9.android.books.R;


public class HomeFragment extends Fragment
implements
        SearchFragment.OnFragmentInteractionListener,
        SearchFragment.OnDataPass,
        WhatsNewFragment.OnFragmentInteractionListener,
        WhatsNewFragment.OnDataPass
{

    private HomeViewModel homeViewModel;
    EditText searchInput;
    ArrayList<String> book1;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
//        final TextView textView = root.findViewById(R.id.text_home);
//        homeViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
//            @Override
//            public void onChanged(@Nullable String s) {
//                textView.setText(s);
//            }
//        });


        Fragment fragment = null;
        fragment = new WhatsNewFragment();
        startFragment(fragment);

        book1 = new ArrayList<String>();
        book1.add("iGen: Why Today’s Super-Connected Kids Are Growing Up Less Rebellious, More Tolerant, Less Happy--and Completely Unprepared for Adulthood--and What That Means for the Rest of Us");
        book1.add("Jean M. Twenge");

        searchInput = root.findViewById(R.id.searchInput);
        searchInput.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode==KeyEvent.KEYCODE_ENTER||keyCode==KeyEvent.KEYCODE_NUMPAD_ENTER){

                //    hideKeyboard(v);

                    search(searchInput.toString(), book1);

                }
                return true;
            }
        });


//        Button btn = root.findViewById(R.id.changeTextBtn);
//        btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                homeViewModel.getText().setValue("Testing it works");
//                startActivity(new Intent(getActivity(),LoginActivity.class));
//            }
//        });
        return root;
    }

    public void search(String search, ArrayList<String> book ){
      // for(int i = 0; i < book.size(); i++){
        //            if( book.get(i).toLowerCase().contains(search.toLowerCase())){
                        Fragment fragment = null;
                        fragment = new SearchFragment();
                        startFragment(fragment);
          //  }

          /*  else{
                        Toast.makeText(getContext(),"Cannot find this book",Toast.LENGTH_LONG).show();
                        // System.out.println("Cannot find this book");
            }

        }*/

    }

    public void startFragment(Fragment f){
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.bottom_fragment, f);
        ft.addToBackStack(null);
        ft.commit();
    }


    @Override
    public void onDataPass(String data) {

    }
    @Override
    public void onFragmentInteraction(Uri uri){
        //empty
    }


    }

