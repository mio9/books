package sh.mio9.android.books;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Menu;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.navigation.NavigationView;

import androidx.annotation.NonNull;
import androidx.core.view.GravityCompat;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import sh.mio9.android.books.data.LoginDataSource;
import sh.mio9.android.books.data.LoginRepository;
import sh.mio9.android.books.ui.contact.ContactFragment;
import sh.mio9.android.books.ui.login.LoginActivity;

public class MainActivity extends AppCompatActivity{

    private AppBarConfiguration mAppBarConfiguration;
    private NavigationView navigationView;
    private LoginRepository loginRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        /*FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });  */
//        fab.setVisibility(View.INVISIBLE);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_categories, R.id.nav_shopping_cart,R.id.nav_contact,R.id.nav_orders)
                .setDrawerLayout(drawer)
                .setFallbackOnNavigateUpListener(new AppBarConfiguration.OnNavigateUpListener() {
                    @Override
                    public boolean onNavigateUp() {
                        return false;
                    }
                })
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

        loginRepository = LoginRepository.getInstance(new LoginDataSource());
//        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
//            @Override
//            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
//                System.out.println("Pressed");
//                switch (item.getItemId()) {
//
//                    case R.id.nav_logout: {
//                        loginRepository.logout();
//                        Toast.makeText(MainActivity.this, "Logged out", Toast.LENGTH_SHORT).show();
//                        break;
//                    }
////                    case R.id.nav_contact:{
////                        getSupportFragmentManager().beginTransaction().replace(R.id.nav_host_fragment,new ContactFragment()).commit();
////                        break;
////                    }
//                }
//                //close navigation drawer
//                DrawerLayout drawer = findViewById(R.id.drawer_layout);
//                drawer.closeDrawer(GravityCompat.START);
//                return true;
//            }
//        });


    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }*/

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);

        View headerView = navigationView.getHeaderView(0);
        TextView navUsername = headerView.findViewById(R.id.navUsername);
        TextView navEmail = headerView.findViewById(R.id.navEmail);

        // Replace text when logged in

        if (loginRepository.isLoggedIn()) {
            //loggwed in
            navUsername.setText(loginRepository.getUser().getDisplayName());
            navEmail.setText(loginRepository.getUser().getEmail());
            headerView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    startActivity(new Intent(getApplicationContext(),LoginActivity.class)); // do nothing now
                }
            });

//            navigationView.getMenu().findItem(R.id.nav_logout).setVisible(true);

        } else {
            //logged out
            navigationView.getMenu().findItem(R.id.nav_logout).setVisible(false);

            navUsername.setText(getString(R.string.nav_username));
            navEmail.setText(getString(R.string.nav_email));
            headerView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //retract the drawer
                    DrawerLayout drawer = findViewById(R.id.drawer_layout);
                    drawer.closeDrawer(GravityCompat.START);
                    startActivity(new Intent(getApplicationContext(),LoginActivity.class));
                }
            });
        }

        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

}
