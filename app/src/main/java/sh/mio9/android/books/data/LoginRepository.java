package sh.mio9.android.books.data;

import java.io.IOException;
import java.util.logging.LoggingMXBean;

import sh.mio9.android.books.data.model.LoggedInUser;

/**
 * Class that requests authentication and user information from the remote data source and
 * maintains an in-memory cache of login status and user credentials information.
 */
public class LoginRepository {

    private static volatile LoginRepository instance;

    private LoginDataSource dataSource;

    // If user credentials will be cached in local storage, it is recommended it be encrypted
    // @see https://developer.android.com/training/articles/keystore
    private LoggedInUser user = null;

    // private constructor : singleton access
    private LoginRepository(LoginDataSource dataSource) {
        this.dataSource = dataSource;
    }

    public static LoginRepository getInstance(LoginDataSource dataSource) {
        if (instance == null) {
            instance = new LoginRepository(dataSource);
        }
        return instance;
    }

    public boolean isLoggedIn() {
        return user != null;
    }

    public void logout() {
        user = null;
        dataSource.logout();
    }

    private void setLoggedInUser(LoggedInUser user) {
        this.user = user;
        // If user credentials will be cached in local storage, it is recommended it be encrypted
        // @see https://developer.android.com/training/articles/keystore
    }

    public LoggedInUser getUser() {
        return user;
    }

    public Result<LoggedInUser> login(String username, String password) {
        // handle login
//        Result<LoggedInUser> result = dataSource.login(username, password);
        Result<LoggedInUser> result;
        if (username.equals("test")&&password.equals("testing")){
            LoggedInUser fakeUser = new LoggedInUser("test","MIO Nekoya");
            result=new Result.Success<>(fakeUser);
        } else {
            result = new Result.Error(new IOException("Error logging in"));
        }
        if (result instanceof Result.Success) {
            setLoggedInUser(((Result.Success<LoggedInUser>) result).getData());
        }
        return result;
    }
}
